﻿using System.Collections.Generic;
using DevelopmentChallenge.Data.Classes;
using DevelopmentChallenge.Data.Classes.Fomas;
using DevelopmentChallenge.Data.Classes.Formas;
using DevelopmentChallenge.Data.Classes.Idiomas;
using NUnit.Framework;

namespace DevelopmentChallenge.Data.Tests
{
    [TestFixture]
    public class DataTests
    {
        [TestCase]
        public void TestResumenListaVacia()
        {
            Assert.AreEqual("<h1>Lista vacía de formas!</h1>",
                FormaGeometrica.Imprimir<Castellano>(new List<FormaGeometrica>()));
        }

        [TestCase]
        public void TestResumenListaVaciaFormasEnIngles()
        {
            Assert.AreEqual("<h1>Empty list of shapes!</h1>",
                FormaGeometrica.Imprimir<Ingles>(new List<FormaGeometrica>()));
        }

        [TestCase]
        public void TestResumenListaConUnCuadrado()
        {
            var cuadrados = new List<FormaGeometrica> { FormaGeometrica.CrearForma( () => new Cuadrado( 5 ) ) };

            var resumen = FormaGeometrica.Imprimir<Castellano>(cuadrados);

            Assert.AreEqual("<h1>Reporte de Formas</h1>1 Cuadrado | Area 25 | Perimetro 20 <br/>TOTAL:<br/>1 formas Perimetro 20 Area 25", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasCuadrados()
        {
            var cuadrados = new List<FormaGeometrica>
            {
                FormaGeometrica.CrearForma(() => new Cuadrado(5)),
                FormaGeometrica.CrearForma(() => new Cuadrado(1)),
                FormaGeometrica.CrearForma(() => new Cuadrado(3))
            };

            var resumen = FormaGeometrica.Imprimir<Ingles>(cuadrados);

            Assert.AreEqual("<h1>Shapes report</h1>3 Squares | Area 35 | Perimeter 36 <br/>TOTAL:<br/>3 shapes Perimeter 36 Area 35", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTipos()
        {
            var formas = new List<FormaGeometrica>
            {
                FormaGeometrica.CrearForma(() => new Cuadrado(5)),
                FormaGeometrica.CrearForma(() => new Circulo(3)),
                FormaGeometrica.CrearForma(() => new TrianguloEquilatero(4)),
                FormaGeometrica.CrearForma(() => new Cuadrado(2)),
                FormaGeometrica.CrearForma(() => new TrianguloEquilatero(9)),
                FormaGeometrica.CrearForma(() => new Circulo(2.75m)),
                FormaGeometrica.CrearForma(() => new TrianguloEquilatero(4.2m)),
            };

            var resumen = FormaGeometrica.Imprimir<Ingles>(formas);

            Assert.AreEqual(
                "<h1>Shapes report</h1>2 Squares | Area 29 | Perimeter 28 <br/>2 Circles | Area 13,01 | Perimeter 18,06 <br/>3 Triangles | Area 49,64 | Perimeter 51,6 <br/>TOTAL:<br/>7 shapes Perimeter 97,66 Area 91,65",
                resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTiposEnCastellano()
        {
            var formas = new List<FormaGeometrica>
            {
                FormaGeometrica.CrearForma(() => new Cuadrado(5)),
                FormaGeometrica.CrearForma(() => new Circulo(3)),
                FormaGeometrica.CrearForma(() => new TrianguloEquilatero(4)),
                FormaGeometrica.CrearForma(() => new Cuadrado(2)),
                FormaGeometrica.CrearForma(() => new TrianguloEquilatero(9)),
                FormaGeometrica.CrearForma(() => new Circulo(2.75m)),
                FormaGeometrica.CrearForma(() => new TrianguloEquilatero(4.2m)),
            };

            var resumen = FormaGeometrica.Imprimir<Castellano>(formas);

            Assert.AreEqual(
                "<h1>Reporte de Formas</h1>2 Cuadrados | Area 29 | Perimetro 28 <br/>2 Círculos | Area 13,01 | Perimetro 18,06 <br/>3 Triángulos | Area 49,64 | Perimetro 51,6 <br/>TOTAL:<br/>7 formas Perimetro 97,66 Area 91,65",
                resumen);
        }

        // NUEVOS TEST

        [TestCase]
        public void TestResumenListaVaciaFormasEnItaliano()
        {
            Assert.AreEqual("<h1>Lista vuota di forme!</h1>",
                 FormaGeometrica.Imprimir<Italiano>(new List<FormaGeometrica>()));
        }


        [TestCase]
        public void TestResumenListaRectangulosCastellano()
        {
            var formas = new List<FormaGeometrica>
            {
                FormaGeometrica.CrearForma(() => new Rectangulo(5, 4)),
                FormaGeometrica.CrearForma(() => new Rectangulo(3, 6)),
                FormaGeometrica.CrearForma(() => new Rectangulo(4, 2)),
            };

            var resumen = FormaGeometrica.Imprimir<Castellano>(formas);

            Assert.AreEqual(
                "<h1>Reporte de Formas</h1>3 Rectángulos | Area 46 | Perimetro 48 <br/>TOTAL:<br/>3 formas Perimetro 48 Area 46",
                resumen);
        }

        [TestCase]
        public void TestResumenListaRectangulosItaliano()
        {
            var formas = new List<FormaGeometrica>
            {
                FormaGeometrica.CrearForma(() => new Rectangulo(5, 4)),
                FormaGeometrica.CrearForma(() => new Rectangulo(3, 6)),
                FormaGeometrica.CrearForma(() => new Rectangulo(4, 2)),
            };

            var resumen = FormaGeometrica.Imprimir<Italiano>(formas);

            Assert.AreEqual(
                "<h1>Rapporto delle forme</h1>3 Rettangoli | Area 46 | Perimetro 48 <br/>TOTAL:<br/>3 forme Perimetro 48 Area 46",
                resumen);
        }

        [TestCase]
        public void TestResumenListaTrapeciosCastellano()
        {
            var formas = new List<FormaGeometrica>
            {
                FormaGeometrica.CrearForma(() => new Trapecio(5, 4, 3)),
                FormaGeometrica.CrearForma(() => new Trapecio(3, 6, 2)),
                FormaGeometrica.CrearForma(() => new Trapecio(4, 6, 7)),
            };

            var resumen = FormaGeometrica.Imprimir<Castellano>(formas);

            Assert.AreEqual(
                "<h1>Reporte de Formas</h1>3 Trapecios | Area 55,24 | Perimetro 56 <br/>TOTAL:<br/>3 formas Perimetro 56 Area 55,24",
                resumen);
        }

        [TestCase]
        public void TestResumenListaTrapeciosItaliano()
        {
            var formas = new List<FormaGeometrica>
            {
                FormaGeometrica.CrearForma(() => new Trapecio(5, 4, 3)),
                FormaGeometrica.CrearForma(() => new Trapecio(3, 6, 2)),
                FormaGeometrica.CrearForma(() => new Trapecio(4, 6, 7)),
            };

            var resumen = FormaGeometrica.Imprimir<Italiano>(formas);

            Assert.AreEqual(
                "<h1>Rapporto delle forme</h1>3 Trapezi | Area 55,24 | Perimetro 56 <br/>TOTAL:<br/>3 forme Perimetro 56 Area 55,24",
                resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTiposEnItaliano()
        {
            var formas = new List<FormaGeometrica>
            {
                FormaGeometrica.CrearForma(() => new Cuadrado(5)),
                FormaGeometrica.CrearForma(() => new Circulo(3)),
                FormaGeometrica.CrearForma(() => new TrianguloEquilatero(4)),
                FormaGeometrica.CrearForma(() => new Cuadrado(2)),
                FormaGeometrica.CrearForma(() => new TrianguloEquilatero(9)),
                FormaGeometrica.CrearForma(() => new Circulo(2.75m)),
                FormaGeometrica.CrearForma(() => new TrianguloEquilatero(4.2m)),
                FormaGeometrica.CrearForma(() => new Rectangulo(5, 4)),
                FormaGeometrica.CrearForma(() => new Rectangulo(3, 6)),
                FormaGeometrica.CrearForma(() => new Trapecio(5, 4, 3)),
                FormaGeometrica.CrearForma(() => new Trapecio(3, 6, 2)),
                FormaGeometrica.CrearForma(() => new Trapecio(4, 6, 7)),
            };

            var resumen = FormaGeometrica.Imprimir<Italiano>(formas);

            Assert.AreEqual(
                "<h1>Rapporto delle forme</h1>2 Quadrati | Area 29 | Perimetro 28 <br/>2 Cerchi | Area 13,01 | Perimetro 18,06 <br/>3 Triangoli | Area 49,64 | Perimetro 51,6 <br/>2 Rettangoli | Area 38 | Perimetro 36 <br/>3 Trapezi | Area 55,24 | Perimetro 56 <br/>TOTAL:<br/>12 forme Perimetro 189,66 Area 184,88",
                resumen);
        }
    }
}
