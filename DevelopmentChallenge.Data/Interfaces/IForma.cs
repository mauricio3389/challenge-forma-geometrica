﻿namespace DevelopmentChallenge.Data.Interfaces
{
    public interface IForma
    {
        decimal CalcularArea();
        decimal CalcularPerimetro();
    }
}
