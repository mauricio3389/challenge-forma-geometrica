﻿using DevelopmentChallenge.Data.Classes.Formas;
using DevelopmentChallenge.Data.Enumerables;

namespace DevelopmentChallenge.Data.Interfaces
{
    public interface IIdioma
    {
        string Traducir( MensajeIdioma mensaje );

        string TraducirForma (FormaEstado estado);
    }
}
