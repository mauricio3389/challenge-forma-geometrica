﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopmentChallenge.Data.Enumerables
{
    public enum MensajeIdioma
    {
        Empty = 0,
        Header = 1,
        Shape = 2,
        Area = 3,
        Perimeter = 4,
    }
}
