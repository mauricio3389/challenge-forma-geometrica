﻿using DevelopmentChallenge.Data.Classes.Fomas;
using DevelopmentChallenge.Data.Classes.Formas;
using DevelopmentChallenge.Data.Enumerables;
using DevelopmentChallenge.Data.Interfaces;
using System;

namespace DevelopmentChallenge.Data.Classes.Idiomas
{
    public class Castellano : IIdioma
    {
        public string Traducir(MensajeIdioma mensaje)
        {
            switch(mensaje)
            {
                case MensajeIdioma.Empty:
                    return "Lista vacía de formas!";
                case MensajeIdioma.Header:
                    return "Reporte de Formas";
                case MensajeIdioma.Shape:
                    return "formas";
                case MensajeIdioma.Area:
                    return "Area";
                case MensajeIdioma.Perimeter:
                    return "Perimetro";
                default:
                    throw new ArgumentOutOfRangeException(@"Mensaje desconocido");
            }
        }

        public string TraducirForma(FormaEstado estado)
        {
            if (estado.Forma == typeof(Cuadrado))
                return estado.Cantidad == 1 ? "Cuadrado" : "Cuadrados";
            if (estado.Forma == typeof(Circulo))
                return estado.Cantidad == 1 ? "Círculo" : "Círculos";
            if (estado.Forma == typeof(TrianguloEquilatero))
                return estado.Cantidad == 1 ? "Triángulo" : "Triángulos";
            if (estado.Forma == typeof(Rectangulo))
                return estado.Cantidad == 1 ? "Rectángulo" : "Rectángulos";
            if (estado.Forma == typeof(Trapecio))
                return estado.Cantidad == 1 ? "Trapecio" : "Trapecios";
            else throw new NotImplementedException("Forma desconocida");
        }
    }
}
