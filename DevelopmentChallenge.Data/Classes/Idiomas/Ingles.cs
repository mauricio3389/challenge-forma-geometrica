﻿using DevelopmentChallenge.Data.Classes.Fomas;
using DevelopmentChallenge.Data.Classes.Formas;
using DevelopmentChallenge.Data.Enumerables;
using DevelopmentChallenge.Data.Interfaces;
using System;

namespace DevelopmentChallenge.Data.Classes.Idiomas
{
    public class Ingles : IIdioma
    {
        public string Traducir(MensajeIdioma mensaje)
        {
            switch (mensaje)
            {
                case MensajeIdioma.Empty:
                    return "Empty list of shapes!";
                case MensajeIdioma.Header:
                    return "Shapes report";
                case MensajeIdioma.Shape:
                    return "shapes";
                case MensajeIdioma.Area:
                    return "Area";
                case MensajeIdioma.Perimeter:
                    return "Perimeter";
                default:
                    throw new ArgumentOutOfRangeException(@"Unknown");
            }
        }

        public string TraducirForma(FormaEstado estado)
        {
            if (estado.Forma == typeof(Cuadrado))
                return estado.Cantidad == 1 ? "Square" : "Squares";
            if (estado.Forma == typeof(Circulo))
                return estado.Cantidad == 1 ? "Circle" : "Circles";
            if (estado.Forma == typeof(TrianguloEquilatero))
                return estado.Cantidad == 1 ? "Triangle" : "Triangles";
            if (estado.Forma == typeof(Rectangulo))
                return estado.Cantidad == 1 ? "Rectangle" : "Rectangles";
            if (estado.Forma == typeof(Trapecio))
                return estado.Cantidad == 1 ? "Trapeze" : "Trapezoids";
            else throw new NotImplementedException("Unknown");
        }
    }
}
