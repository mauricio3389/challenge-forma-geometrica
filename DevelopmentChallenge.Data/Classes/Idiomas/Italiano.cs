﻿using DevelopmentChallenge.Data.Classes.Fomas;
using DevelopmentChallenge.Data.Classes.Formas;
using DevelopmentChallenge.Data.Enumerables;
using DevelopmentChallenge.Data.Interfaces;
using System;

namespace DevelopmentChallenge.Data.Classes.Idiomas
{
    public class Italiano : IIdioma
    {
        public string Traducir(MensajeIdioma mensaje)
        {
            switch (mensaje)
            {
                case MensajeIdioma.Empty:
                    return "Lista vuota di forme!";
                case MensajeIdioma.Header:
                    return "Rapporto delle forme";
                case MensajeIdioma.Shape:
                    return "forme";
                case MensajeIdioma.Area:
                    return "Area";
                case MensajeIdioma.Perimeter:
                    return "Perimetro";
                default:
                    throw new ArgumentOutOfRangeException(@"Sconosciuto");
            }
        }

        public string TraducirForma(FormaEstado estado)
        {
            if (estado.Forma == typeof(Cuadrado))
                return estado.Cantidad == 1 ? "Quadrato" : "Quadrati";
            if (estado.Forma == typeof(Circulo))
                return estado.Cantidad == 1 ? "Cerchio" : "Cerchi";
            if (estado.Forma == typeof(TrianguloEquilatero))
                return estado.Cantidad == 1 ? "Triangolo" : "Triangoli";
            if (estado.Forma == typeof(Rectangulo))
                return estado.Cantidad == 1 ? "Rettangolo" : "Rettangoli";
            if (estado.Forma == typeof(Trapecio))
                return estado.Cantidad == 1 ? "Trapezio" : "Trapezi";
            else throw new NotImplementedException("Sconosciuto");
        }
    }
}
