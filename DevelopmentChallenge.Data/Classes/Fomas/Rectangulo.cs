﻿using DevelopmentChallenge.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopmentChallenge.Data.Classes.Fomas
{
    public class Rectangulo : IForma
    {
        private readonly decimal _alto;
        private readonly decimal _ancho;

        /// <summary>
        /// Polígono de cuatro lados con ángulos rectos (90 grados) y lados opuestos de igual longitud
        /// </summary>
        /// <param name="alto">Laterales del rectángulo</param>
        /// <param name="ancho">base y techo del rectángulo</param>
        public Rectangulo(decimal alto, decimal ancho)
        {
            _alto = alto;
            _ancho = ancho;
        }

        public decimal CalcularArea() => this._alto * this._ancho;
        public decimal CalcularPerimetro() => (this._alto * 2) + (this._ancho * 2);
    }
}
