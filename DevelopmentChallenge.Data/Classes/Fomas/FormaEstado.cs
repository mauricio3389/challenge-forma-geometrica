﻿using DevelopmentChallenge.Data.Interfaces;
using System;

namespace DevelopmentChallenge.Data.Classes.Formas
{
    public class FormaEstado
    {
        public int Cantidad {  get; set; }
        public decimal Perimetro {  get; set; }
        public decimal Area {  get; set; }
        public Type Forma {  get; set; }
    }
}
