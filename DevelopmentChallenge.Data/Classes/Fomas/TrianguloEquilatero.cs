﻿using DevelopmentChallenge.Data.Interfaces;
using System;

namespace DevelopmentChallenge.Data.Classes.Fomas
{
    public class TrianguloEquilatero : IForma
    {
        private readonly decimal _lado;

        /// <summary>
        /// Triángulo en el que todos los lados tienen la misma longitud
        /// </summary>
        /// <param name="lado">Lado del triángulo</param>
        public TrianguloEquilatero(decimal lado)
        {
            _lado = lado;
        }

        public decimal CalcularArea() => ((decimal)Math.Sqrt(3) / 4) * _lado * _lado;
        public decimal CalcularPerimetro() => _lado * 3;
    }
}
