﻿using DevelopmentChallenge.Data.Interfaces;
using System;

namespace DevelopmentChallenge.Data.Classes.Fomas
{
    public class Trapecio : IForma
    {
        private readonly decimal _piso;
        private readonly decimal _lado;
        private readonly decimal _techo;
        private readonly decimal _altura;

        /// <summary>
        /// Polígono de cuatro lados con al menos un par de lados paralelos
        /// </summary>
        /// <param name="piso">Base del trapacio</param>
        /// <param name="lado">Laterales del trapecio</param>
        /// <param name="techo">Techo del trapecio</param>
        public Trapecio(decimal piso, decimal lado, decimal techo)
        {
            _piso = piso;
            _lado = lado;
            _techo = techo;
            _altura = (decimal)Math.Sqrt(Math.Pow((double)_lado, 2) - Math.Pow(((double)_techo - (double)_piso + Math.Pow((double)_lado, 2)) / (2 * (double)_lado), 2));
        }

        public decimal CalcularArea() => (this._piso + this._techo) * this._altura / 2;
        public decimal CalcularPerimetro() => this._piso + this._techo + 2 * this._lado;
    }
}
