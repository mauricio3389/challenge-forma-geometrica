﻿using DevelopmentChallenge.Data.Interfaces;

namespace DevelopmentChallenge.Data.Classes.Formas
{
    public class Cuadrado : IForma
    {
        private readonly decimal _lado;

        /// <summary>
        /// Polígono de cuatro lados donde todos los lados tienen la misma longitud
        /// </summary>
        /// <param name="lado">Lado del cuadrado</param>
        public Cuadrado(decimal lado)
        {
            _lado = lado;
        }

        public decimal CalcularArea() => this._lado * this._lado;
        public decimal CalcularPerimetro() => this._lado * 4;
    }
}
