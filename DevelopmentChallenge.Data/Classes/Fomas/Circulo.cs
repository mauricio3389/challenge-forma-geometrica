﻿using DevelopmentChallenge.Data.Interfaces;
using System;

namespace DevelopmentChallenge.Data.Classes.Fomas
{
    public class Circulo : IForma
    {
        private readonly decimal _lado;

        /// <summary>
        /// Conjunto de todos los puntos en un plano que están a una distancia fija de un punto fijo
        /// </summary>
        /// <param name="lado">Diámetro del círculo</param>
        public Circulo(decimal lado)
        {
            _lado = lado;
        }

        public decimal CalcularArea() => (decimal)Math.PI * (_lado / 2) * (_lado / 2);
        public decimal CalcularPerimetro() => (decimal)Math.PI * _lado;
    }
}
