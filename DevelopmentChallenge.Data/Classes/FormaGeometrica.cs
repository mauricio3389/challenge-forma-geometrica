﻿/******************************************************************************************************************/
/******* ¿Qué pasa si debemos soportar un nuevo idioma para los reportes, o agregar más formas geométricas? *******/
/******************************************************************************************************************/

/*
 * TODO: 
 * Refactorizar la clase para respetar principios de la programación orientada a objetos.
 * Implementar la forma Trapecio/Rectangulo. 
 * Agregar el idioma Italiano (o el deseado) al reporte.
 * Se agradece la inclusión de nuevos tests unitarios para validar el comportamiento de la nueva funcionalidad agregada (los tests deben pasar correctamente al entregar la solución, incluso los actuales.)
 * Una vez finalizado, hay que subir el código a un repo GIT y ofrecernos la URL para que podamos utilizar la nueva versión :).
 */

using DevelopmentChallenge.Data.Interfaces;
using DevelopmentChallenge.Data.Enumerables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevelopmentChallenge.Data.Classes.Formas;

namespace DevelopmentChallenge.Data.Classes
{
    public class FormaGeometrica
    {
        private readonly IForma _tipo;

        public FormaGeometrica(IForma forma = null)
        {
            _tipo = forma;
        }

        public static FormaGeometrica CrearForma<T>(Func<T> func) where T : IForma
        {
            return new FormaGeometrica(func());
        }

        public static string Imprimir<R>(List<FormaGeometrica> formas) where R : IIdioma, new()
        {
            R idioma = new R();

            var sb = new StringBuilder();

            if (!formas.Any())
            {
                sb.Append($"<h1>{idioma.Traducir(MensajeIdioma.Empty)}</h1>");
            }
            else
            {
                // Hay por lo menos una forma
                // HEADER
                sb.Append($"<h1>{idioma.Traducir(MensajeIdioma.Header)}</h1>");

                var grupoForma = new List<FormaEstado>();

                foreach (var forma in formas)
                {
                    FormaEstado estado = grupoForma
                                        .Where(x => x.Forma == forma._tipo.GetType())
                                        .FirstOrDefault();

                    if (estado == null)
                    {
                        estado = new FormaEstado();
                        estado.Forma = forma._tipo.GetType();
                        grupoForma.Add(estado);
                    }

                    estado.Cantidad++;
                    estado.Area += forma.CalcularArea();
                    estado.Perimetro += forma.CalcularPerimetro();
                }

                int totalFormas = 0;
                decimal totalPerimetro = 0m;
                decimal totalArea = 0m;

                foreach (var estado in grupoForma)
                {
                    sb.Append(ObtenerLinea(estado, idioma));

                    totalFormas += estado.Cantidad;
                    totalPerimetro += estado.Perimetro;
                    totalArea += estado.Area;
                }

                // FOOTER
                sb.Append("TOTAL:<br/>");
                sb.Append(totalFormas + " " + idioma.Traducir(MensajeIdioma.Shape) + " ");
                sb.Append(idioma.Traducir(MensajeIdioma.Perimeter) + " " + totalPerimetro.ToString("#.##") + " ");
                sb.Append(idioma.Traducir(MensajeIdioma.Area) + " " + totalArea.ToString("#.##"));
            }

            return sb.ToString();
        }

        private static string ObtenerLinea<R>(FormaEstado estado, R idioma) where R : IIdioma
        {
            if (estado.Cantidad > 0)
            {
                return $"{estado.Cantidad} { idioma.TraducirForma(estado) } | { idioma.Traducir(MensajeIdioma.Area) } {estado.Area:#.##} | {idioma.Traducir(MensajeIdioma.Perimeter)} {estado.Perimetro:#.##} <br/>";
            }

            return string.Empty;
        }

        public decimal CalcularArea()
        {
            return this._tipo.CalcularArea();
        }

        public decimal CalcularPerimetro()
        {
            return this._tipo.CalcularPerimetro();
        }
    }
}
