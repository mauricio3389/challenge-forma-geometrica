# Challenge FormaGeometrica Mauricio Torres Ramos



## Crear nuevas formas geométricas

Para crear una nueva forma geométrica deben crear una clase dentro de Classes/Formas e implementar la interface ``IForma`` lo cual requiere crear 2 funciones, ``CalcularArea()`` y ``CalcularPerimetro()`` <br>
Además, deben agregar el constructor que reciba los valores necesarios para calcular el área y el perímetro.

``` cs
public class Rectangulo : IForma
{
    private readonly decimal _alto;
    private readonly decimal _ancho;

    /// <summary>
    /// Polígono de cuatro lados con ángulos rectos (90 grados) y lados opuestos de igual longitud
    /// </summary>
    /// <param name="alto">Laterales del rectángulo</param>
    /// <param name="ancho">base y techo del rectángulo</param>
    public Rectangulo(decimal alto, decimal ancho)
    {
        _alto = alto;
        _ancho = ancho;
    }

    public decimal CalcularArea() => this._alto * this._ancho;
    public decimal CalcularPerimetro() => (this._alto * 2) + (this._ancho * 2);
}
```

## Crear nuevos idiomas

Para crear un nuevo idioma deben crear una clase dentro de Classes/Idiomas e implementar la interface ``IIdioma`` lo cual requiere crear 2 funciones ``Traducir(MensajeIdioma mensaje)`` y ``TraducirForma(FormaEstado estado)`` donde se puede aplicar los textos en el idioma de la clase.

``` cs
public class Italiano : IIdioma
{
    public string Traducir(MensajeIdioma mensaje)
    {
        switch (mensaje)
        {
            case MensajeIdioma.Empty:
                return "Lista vuota di forme!";
            case MensajeIdioma.Header:
                return "Rapporto delle forme";
            case MensajeIdioma.Shape:
                return "forme";
            case MensajeIdioma.Area:
                return "Area";
            case MensajeIdioma.Perimeter:
                return "Perimetro";
            default:
                throw new ArgumentOutOfRangeException(@"Sconosciuto");
        }
    }

    public string TraducirForma(FormaEstado estado)
    {
        if (estado.Forma == typeof(Cuadrado))
            return estado.Cantidad == 1 ? "Quadrato" : "Quadrati";
        if (estado.Forma == typeof(Circulo))
            return estado.Cantidad == 1 ? "Cerchio" : "Cerchi";
        if (estado.Forma == typeof(TrianguloEquilatero))
            return estado.Cantidad == 1 ? "Triangolo" : "Triangoli";
        if (estado.Forma == typeof(Rectangulo))
            return estado.Cantidad == 1 ? "Rettangolo" : "Rettangoli";
        if (estado.Forma == typeof(Trapecio))
            return estado.Cantidad == 1 ? "Trapezio" : "Trapezi";
        else throw new NotImplementedException("Sconosciuto");
    }
}
```

Tener en cuenta que por cada nueva forma geométrica, se debe modificar la función ``TraducirForma`` de cada idioma para que reconozca la nueva forma geométrica.

## Uso de las formas gemétricas

Para utilizar las formas geométricas y agregarlas en una lista, se implementó una nueva función dentro de ``FormaGeometrica`` llamada ``CrearForma`` y como parámetro recibe un  ``Func`` donde se crea la forma solicitada y sus parámetros requeridos.

``new Cuadrado()`` recibe un parámetro dado que todos sus lados son iguales:

``` cs
var cuadrados = new List<FormaGeometrica>
{
    FormaGeometrica.CrearForma(() => new Cuadrado(5)),
    FormaGeometrica.CrearForma(() => new Cuadrado(1)),
    FormaGeometrica.CrearForma(() => new Cuadrado(3))
};
```
``new Rectangulo()`` recibe 2 parámetros por la base y altura del rectángulo:

``` cs
var formas = new List<FormaGeometrica>
{
    FormaGeometrica.CrearForma(() => new Rectangulo(5, 4)),
    FormaGeometrica.CrearForma(() => new Rectangulo(3, 6)),
    FormaGeometrica.CrearForma(() => new Rectangulo(4, 2)),
};
```
Se puede combinar tipos distintas formas geompetricas en una misma lista, cada una con sus propios parámetros:

``` cs
var formas = new List<FormaGeometrica>
{
    FormaGeometrica.CrearForma(() => new Cuadrado(5)),
    FormaGeometrica.CrearForma(() => new Circulo(3)),
    FormaGeometrica.CrearForma(() => new TrianguloEquilatero(4)),
    FormaGeometrica.CrearForma(() => new Cuadrado(2)),
    FormaGeometrica.CrearForma(() => new TrianguloEquilatero(9)),
    FormaGeometrica.CrearForma(() => new Circulo(2.75m)),
    FormaGeometrica.CrearForma(() => new TrianguloEquilatero(4.2m)),
    FormaGeometrica.CrearForma(() => new Rectangulo(5, 4)),
    FormaGeometrica.CrearForma(() => new Rectangulo(3, 6)),
    FormaGeometrica.CrearForma(() => new Trapecio(5, 4, 3)),
    FormaGeometrica.CrearForma(() => new Trapecio(3, 6, 2)),
    FormaGeometrica.CrearForma(() => new Trapecio(4, 6, 7)),
};
```

## Imprimir en idiomas

Cuando se desea llama a la función ``FormaGeometrica.Imprimir`` se debe pasar como parámetro de tipo el idioma que se desa obtener el resultado, y las formas geométricas

``` cs
var resumen = FormaGeometrica.Imprimir<Castellano>(formas);
```

o en otros idiomas ya configurados:

``` cs
var resumen = FormaGeometrica.Imprimir<Ingles>(formas);
```

``` cs
var resumen = FormaGeometrica.Imprimir<Italiano>(formas);
```

## Ejemplos

Lista vacía en Italiano
``` cs
public void TestResumenListaVaciaFormasEnItaliano()
{
    Assert.AreEqual("<h1>Lista vuota di forme!</h1>",
         FormaGeometrica.Imprimir<Italiano>(new List<FormaGeometrica>()));
}
```

1 Cuadrado en Italiano

``` cs
 public void TestResumenListaConUnCuadradoItaliano()
 {
     var cuadrados = new List<FormaGeometrica> { FormaGeometrica.CrearForma(() => new Cuadrado(5)) };

     var resumen = FormaGeometrica.Imprimir<Italiano>(cuadrados);

     Assert.AreEqual("<h1>Rapporto delle forme</h1>1 Quadrato | Area 25 | Perimetro 20 <br/>TOTAL:<br/>1 forme Perimetro 20 Area 25", resumen);
 }
```

Multiples Formas en Italiano

``` cs
public void TestResumenListaConMasTiposEnItaliano()
{
    var formas = new List<FormaGeometrica>
    {
        FormaGeometrica.CrearForma(() => new Cuadrado(5)),
        FormaGeometrica.CrearForma(() => new Circulo(3)),
        FormaGeometrica.CrearForma(() => new TrianguloEquilatero(4)),
        FormaGeometrica.CrearForma(() => new Cuadrado(2)),
        FormaGeometrica.CrearForma(() => new TrianguloEquilatero(9)),
        FormaGeometrica.CrearForma(() => new Circulo(2.75m)),
        FormaGeometrica.CrearForma(() => new TrianguloEquilatero(4.2m)),
        FormaGeometrica.CrearForma(() => new Rectangulo(5, 4)),
        FormaGeometrica.CrearForma(() => new Rectangulo(3, 6)),
        FormaGeometrica.CrearForma(() => new Trapecio(5, 4, 3)),
        FormaGeometrica.CrearForma(() => new Trapecio(3, 6, 2)),
        FormaGeometrica.CrearForma(() => new Trapecio(4, 6, 7)),
    };

    var resumen = FormaGeometrica.Imprimir<Italiano>(formas);

    Assert.AreEqual(
        "<h1>Rapporto delle forme</h1>2 Quadrati | Area 29 | Perimetro 28 <br/>2 Cerchi | Area 13,01 | Perimetro 18,06 <br/>3 Triangoli | Area 49,64 | Perimetro 51,6 <br/>2 Rettangoli | Area 38 | Perimetro 36 <br/>3 Trapezi | Area 55,24 | Perimetro 56 <br/>TOTAL:<br/>12 forme Perimetro 189,66 Area 184,88",
        resumen);
}
```